
setenv CERN_ROOT $EICDIRECTORY/gcc83lib
setenv LHAPDF5 $EICDIRECTORY/gcc83lib
setenv LD_LIBRARY_PATH ${LHAPDF5}
source /cvmfs/sphenix.sdcc.bnl.gov/gcc-8.3/opt/sphenix/core/gcc/8.3.0.1-0a5ad/x86_64-centos7/setup.csh
source /cvmfs/sphenix.sdcc.bnl.gov/gcc-8.3/opt/sphenix/core/binutils/2.30-e5b21/x86_64-centos7/setup.csh

## Use the following to prepare before running
# cp $FLUPRO/nuclear.bin .
# cp $EICDIRECTORY/PACKAGES/DPMJetHybrid/eAS1tst .
# cp $EICDIRECTORY/PACKAGES/DPMJetHybrid/eAu_shorttest_Shd1_tau9_kt=ptfrag=0.40.inp .
# mkdir outForPythiaMode
# ln -s $EICDIRECTORY/PACKAGES/DPMJetHybrid/PyQM

$EICDIRECTORY/PACKAGES/dpmjetHybrid < eAu_10k_Shd1_tau9_kt=ptfrag=0.32.inp < eAu_shorttest_Shd1_tau9_kt=ptfrag=0.40.inp > eAu_shorttest_Shd1_tau9_kt=ptfrag=0.40.log
