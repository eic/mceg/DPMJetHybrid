*These are the interfaces to pythia needed by dpmjet

C...intialize pythia when using dpmjet
*======================================================================
      subroutine DT_PYINITEP(EPN,PPN,Q2MIN,Q2MAX,YMIN,YMAX,INPUT)
*     input:
*           EPN      electron beam momentum in lab frame
*           PPN      proton beam momentum in lab frame
*           Q2MIN    Q2 cut low
*           Q2MAX    Q2 cut high
*           YMIN     Y cut low
*           YMAX     Y cut high

      include 'pythia.inc'              ! All PYTHIA commons blocks
      include "mc_set.inc"
      include "py6strf.inc"
      include "mcRadCor.inc"
      include "radgen.inc"
      include "phiout.inc"

* properties of interacting particles
      COMMON /DTPRTA/ IT,ITZ,IP,IPZ,IJPROJ,IBPROJ,IJTARG,IBTARG

c...target/proj mass, charge and projectile internal ID
      integer IT, ITZ, IP, IPZ, IJPROJ

      double precision EPN,PPN
      PARAMETER (ZERO=0.0D0,ONE=1.0D0,TINY10=1.0D0-10,MAXNCL=260)
C...Added by liang 1/6/12
C...Switches for nuclear correction
      COMMON /PYNUCL/ INUMOD,CHANUM,ORDER,genShd
      SAVE /PYNUCL/
      DOUBLE PRECISION INUMOD,CHANUM
      INTEGER ORDER,genShd

C...Parameters and switch for energy loss
      DOUBLE PRECISION QHAT
      INTEGER QSWITCH
      COMMON /QUENCH/ QHAT, QSWITCH

C...Information about LT from lab to nucleon rest frame
      COMMON /LABTONR/ pgamma, pbeta

C...output file name definition
      COMMON /OUNAME/ outname

      integer NEV, NPRT, ievent, genevent, I, tracknr, ltype 
      integer lastgenevent, idum1, idum2, initseed, nrtrack
      REAL trueX, trueW2, trueNu
      DOUBLE PRECISION sqrts, radgamE, radgamp, radgamEnucl
      DOUBLE PRECISION pbeamE, pbeta, pgamma, ebeamE, epznucl
      CHARACTER PARAM*100
      LOGICAL UseLut, GenLut

      ! beam type
      CHARACTER*10 lName, tName
c ---------------------------------------------------------------------
c     Run parameter
c ---------------------------------------------------------------------
      integer*4 today(3), now(3)
c---------------------------------------------------------------------
c     ASCII output file and input file
c ---------------------------------------------------------------------
      CHARACTER*8 INPUT
      CHARACTER*256 outputfilename
      CHARACTER*256 outname

      integer LINP
      parameter ( LINP=28 )
      CHARACTER*256 inputfilename
c---------------------------------------------------------------------
! ... force block data modules to be read
C       external pydata
c ---------------------------------------------------------------------

       iModel=0
       pbeam=real(PPN)
       ebeam=real(EPN) 
       etype=11
       masse=PYMASS(11)
       massp=PYMASS(2212)
       ievent=0
       genevent=0
       lastgenevent=0
       tracknr=0
c ---------------------------------------------------------------------
c     Open ascii input file
c ---------------------------------------------------------------------
       inputfilename=INPUT
       open(LINP, file=inputfilename,STATUS='UNKNOWN')
       write(*,*) 'the input file is: ', inputfilename

C...Read output file name
       READ(LINP,*) outname
C...Read min/max x of radgen lookup table
       READ(LINP,*) mcSet_XMin, mcSet_XMax
C...Read information for cross section used in radgen
       READ(LINP,*) genSet_FStruct, genSet_R
C...Read parameters of radcorr: do radcorr (1), generate look-up table (2)
       READ(LINP,*) qedrad
C...Read parameters for PYTHIA-Model = which generation is done     
       READ(LINP,*) iModel
C...Read target type mass and charge
       READ(LINP,*) mcSet_TarA, mcSet_TarZ
C...Read switch for shadowing
       READ(LINP,*) genShd
C...Read nuclear pdf correction order
       READ(LINP,*) ORDER
C...Read the switch for quenching
       READ(LINP,*) QSWITCH
C...Read q hat
       READ(LINP,*) QHAT
C...Read information for cross section used in radgen
  100  READ(LINP,'(A)',END=200) PARAM
       CALL PYGIVE(PARAM)
       GOTO 100
c ---------------------------------------------------------------------
C...Initialize PYTHIA.      
c ---------------------------------------------------------------------
  200  CLOSE(LINP)
c...read parameters from dpmjet       
       INUMOD=IT
       CHANUM=ITZ
       mcSet_YMin=real(YMIN)
       mcSet_YMax=real(YMAX)
       mcSet_Q2Min=real(Q2MIN)
       mcSet_Q2Max=real(Q2MAX)

       write(*,*) '*********************************************'
       write(*,*) 'NOW all parameters are read by PYTHIA'
       write(*,*) '*********************************************'
       write(*,*) 'the output file is: ', outname
C       call PYLIST(11)
C       call PYLIST(12)

       print*,'kinematics cut read by PYTHIA:'
       print*,YMIN,' < y < ',YMAX,', ',Q2MIN,' < Q2 < ',Q2MAX

C     Getting the date and time of the event generation
        
      call idate(today)   ! today(1)=day, (2)=month, (3)=year
      call itime(now)     ! now(1)=hour, (2)=minute, (3)=second
        
!     Take date as the SEED for the random number generation
       
C...The random number in pythia has been silenced. Use only that
C   FLUKA which is initialized in dpmjet
      initseed = today(1) + 10*today(2) + today(3) + now(1) + 5*now(3)
c      initseed = 1
      write(6,*) 'SEED = ', initseed
      call rndmq (idum1,idum2,initseed,' ')
        

C     proton is defined in positive z and as target
      P(2,1)=0.0  
      P(2,2)=0.0  
      P(2,3)=pbeam
C     lepton is defined in negative z and as beam
      P(1,1)=0.0  
      P(1,2)=0.0  
      P(1,3)=-ebeam

      !default lName and tName
      lName = 'gamma/e-'
      tName = 'p+'

      !set up lepton beam
      if( IJPROJ.eq.3 ) then
         lName = 'gamma/e-'
         ltype = 11
      elseif( IJPROJ.eq.4 ) then
         lName = 'gamma/e+'
         ltype = -11
      elseif( IJPROJ.eq.10 ) then
         lName = 'gamma/mu+'
         ltype = -13
      elseif( IJPROJ.eq.11 ) then
         lName = 'gamma/mu-'
         ltype = 13
      endif

      !set up nucleon beam
      if( mcSet_TarZ.eq.1 ) then
         tName = 'p+'
      elseif( mcSet_TarZ.eq.0 ) then
         tName = 'n0'
      endif


      if (mcSet_TarZ.eq.0) then
        massp=PYMASS(2112)
      else
        massp=PYMASS(2212)
      endif
      masse=PYMASS(ltype)

      pbeamE=sqrt(pbeam**2+massp**2)
      pbeta=pbeam/pbeamE
      pgamma=pbeamE/massp
      ebeamE=sqrt(ebeam**2+masse**2)
      ebeamEnucl=pgamma*ebeamE-pgamma*pbeta*(-ebeam)
      epznucl=-pgamma*pbeta*(ebeamE)+pgamma*(-ebeam)
      write(*,*) ebeamEnucl, ebeamE, epznucl, -ebeam
      mcSet_EneBeam=sngl(ebeamEnucl)

      sqrts=sqrt((pbeamE+ebeamE)**2-(pbeam-ebeam)**2)
      write(*,*) '*********************************************'
      write(*,*) 'proton beam energy:', pbeamE, 'GeV'
      write(*,*) 'lepton beam energy:', ebeamE, 'GeV'
      write(*,*) 'resulting sqrt(s):', sqrts, 'GeV'
      write(*,*) '*********************************************'

       if (iModel.eq.0) then
           UseLUT=.false.
           GenLUT=.false.
           qedrad=0
           MSTP(199)=0
           mcRadCor_EBrems=0.
       elseif (iModel.eq.1) then
         if (qedrad.eq.0) then
             mcRadCor_EBrems=0.
             UseLUT=.false.
             GenLUT=.false.
             MSTP(199)=1
         elseif (qedrad.eq.1) then
             mcRadCor_EBrems=0.
             UseLUT=.true.
             GenLUT=.false.
             MSTP(199)=1
             call radgen_init(UseLUT,GenLUT)
             write(*,*) 'I have initialized radgen'
         elseif (qedrad.eq.2) then
             write(*,*) 'radgen lookup table will be generated'
             mcRadCor_EBrems=0.
             UseLUT=.true.
             GenLUT=.true.
             MSTP(199)=1
             call radgen_init(UseLUT,GenLUT)
             goto 500
         endif
       endif

      IF(QSWITCH.EQ.1) THEN
         print*,'Quenching requested, qhat=',QHAT
         call GenNucDens(int(CHANUM), int(INUMOD))
c...when quenching is used switch off internal parton shower
         MSTP(61)=0
         MSTP(71)=0
      ENDIF



c     Initialize according to the incoming particle type
c      if ((mcSet_TarZ.eq.1).and.(ltype.eq.11)) then
c         call pyinit ('3MOM','gamma/e-','p+',WIN)
c      elseif ((mcSet_TarZ.eq.1).and.(ltype.eq.-11)) then
c         call pyinit ('3MOM','gamma/e+','p+',WIN)
c      elseif ((mcSet_TarZ.eq.0).and.(ltype.eq.-11)) then
c         call pyinit ('3MOM','gamma/e+','n0',WIN)
c      elseif ((mcSet_TarZ.eq.0).and.(ltype.eq.11)) then
c         call pyinit ('3MOM','gamma/e-','n0',WIN)
c      endif


      !initialize pythia
      call pyinit('3MOM', lName, tName, WIN)


C      If we ever want to simulate fixed target we need to change this
C      win=ebeam
C      call pyinit('fixt','gamma/e-','p+', WIN)



  500  if (qedrad.eq.2) then
         write(*,*) 'lookup table is generated;'
         write(*,*) 'to run now pythia change parameter qedrad to 1'
       endif

      RETURN
      END

*=====dt_pyevnt========================================================
*used to the sample a pythia event and dump this event to DTEVT1
*Some important thing to be mentioned here is the output of pyhia
*data is in the lab frame, while the calculation in dpmjet is made
*in photon-proton c.m.s frame. So when we are copying the data common
*to dpmjet, we must make the right Lorentz transformation. Plus,
*the virtual photon has to be directed to the z+ direction. A rotation
*for the reference is also necessary.

      SUBROUTINE DT_PYEVNTEP(Q2,YY,MODE,IREJ)
 
*     input:
*           MODE  1: generate a pythia event get its Q2 and Y
*                 2: copy this event to dpmjet data common block
*     output:
*           Q2    Q2 of this current event (used in MODE 1)
*           YY    Y of this current event  (used in MODE 1)
*           IREJ  reject flag for current event (used in MODE 2)

      include 'pythia.inc'              ! All PYTHIA commons blocks
      include "mc_set.inc"
      include "py6strf.inc"
      include "mcRadCor.inc"
      include "radgen.inc"
      include "phiout.inc"
* event flag
      COMMON /DTEVNO/ NEVENT,ICASCA 

* event history

      PARAMETER (NMXHKK=200000)

      COMMON /DTEVT1/ NHKK,NEVHKK,ISTHKK(NMXHKK),IDHKK(NMXHKK),
     &                JMOHKK(2,NMXHKK),JDAHKK(2,NMXHKK),
     &                PHKK(5,NMXHKK),VHKK(4,NMXHKK),WHKK(4,NMXHKK)

* extended event history
      COMMON /DTEVT2/ IDRES(NMXHKK),IDXRES(NMXHKK),NOBAM(NMXHKK),
     &                IDBAM(NMXHKK),IDCH(NMXHKK),NPOINT(10)
c     &                IHIST(2,NMXHKK)

      PARAMETER (ZERO=0.0D0,ONE=1.0D0,TINY10=1.0D0-10,MAXNCL=260)

C...Information about LT from lab to nucleon rest frame
      COMMON /LABTONR/ pgamma, pbeta

C...Parameters and switch for energy loss from PyQM
      DOUBLE PRECISION QHAT
      INTEGER QSWITCH
      COMMON /QUENCH/ QHAT, QSWITCH

* Lorentz-parameters of the current interaction from DPMJET
      COMMON /DTLTRA/ GACMS(2),BGCMS(2),GALAB,BGLAB,BLAB,
     &                UMO,PPCM,EPROJ,PPROJ

* lorentz transformation parameter 
      COMMON /LTPARA/ BGTA(4), GAMM(5), eveBETA(4), GAA
      COMMON /ROTATE/ COF,COD,SIF,SID

* properties of photon/lepton projectiles from DPMJET
      COMMON /DTGPRO/ VIRT,PGAMM(4),PLEPT0(4),PLEPT1(4),PNUCL(4),IDIREC

* kinematics at lepton-gamma vertex from DPMJET
      COMMON /DTLGVX/ PPL0(4),PPL1(4),PPG(4),PPA(4)

* position of interacted nucleon
      DOUBLE PRECISION PosNuc
      COMMON /NPARINT/ PosNuc(4)

      double precision PF
      COMMON /CORECT/ PF(4)
      double precision PAUX
      COMMON /PFAUX/ PAUX(4)

      DOUBLE PRECISION P1, P2, P3, P4, PTOT, PP1, PP2, PP3, PP4
      DOUBLE PRECISION pbeta


      INTEGER MODE

      DOUBLE PRECISION Q2, YY

      LOGICAL LFIRST
      INTEGER IREJ

      SAVE LFIRST

      GOTO(1,2) MODE

c...generate a pythia event
1     CONTINUE      
c...if quenching requested, do not do fragmentation
      if(QSWITCH.EQ.1) MSTJ(1) = 0
999   CALL PYEVNT
      IF(MSTI(61).EQ.1) THEN
         WRITE(*,*) 'go back to PYEVNT call'
         GOTO 999
      ENDIF

      Q2=VINT(307)
      YY=VINT(309)
c      print*,'in event:', NEVENT
c      print*,'Q2 from pythia:',Q2
c      print*,'Y from pythia:',YY

c      CALL PYLIST(2)
      LFIRST=.FALSE.

      RETURN

c...transform this event to certain reference and copy it to DPMJET
2     CONTINUE
c...guess the position of final state particle
c...NPOINT(4), the position where final particle starts is very important!!      
      IREJ=0

c      IF(LFIRST) GOTO 9999

c...added by liang for test
c      print*,'NPOINT(1)=',NPOINT(1)

      NPOINT(1)=NHKK-1
      NPOINT(2)=NPOINT(1)
      NPOINT(3)=NPOINT(1)
      NPOINT(4)=NPOINT(1)+5

********find the position of wounded nucleon
      DO 30 J=1,NHKK      
            IF(ISTHKK(J).EQ.12) THEN
               PosNuc(1)=VHKK(1,J)
               PosNuc(2)=VHKK(2,J)
               PosNuc(3)=VHKK(3,J)
               PosNuc(4)=VHKK(4,J)
               GOTO 31
            ENDIF
30    CONTINUE      
31    CONTINUE
c      write(*,*) 'DPMJET position',PosNuc(1),PosNuc(2),PosNuc(3)
      PF(1)=0
      PF(2)=0
      PF(3)=0
      PF(4)=0

c...do quenching to scattered partons if requested      
      if(QSWITCH.EQ.1) then
         CALL PYROBO(0,0,0.0D0,0.0D0,0.0D0,0.0D0,-pbeta)
         call InterPos ! Pick the position of the interaction in the nuclei
         call ApplyQW(QHAT)  ! Compute QW
         CALL PYROBO(0,0,0.0D0,0.0D0,0.0D0,0.0D0,pbeta)
         MSTJ(1) = 1
         CALL PYEXEC
c         CALL PYLIST(2)
      endif

      P3=PF(3)
      P4=PF(4)
      PF(3)=pgamma*(P3+pbeta*P4)
      PF(4)=pgamma*(P4+pbeta*P3)
c      write(*,*) PF(1),PF(2),PF(3),PF(4)

c...Translate PYJETS into HEPEVT event record
      CALL PYHEPC(1)

c...Output part
c...First loop to find exchanged boson
      DO  J=1,NHEP
c...output of event list from pythia for a temp check      
c      WRITE(88,998) J,ISTHEP(J),IDHEP(J),JMOHEP(1,J),
c     &          JMOHEP(2,J),JDAHEP(1,J),JDAHEP(2,J),
c     &          PHEP(1,J),PHEP(2,J),PHEP(3,J),PHEP(4,J),
c     &              PHEP(5,J)
c  998      FORMAT(I5,I5,I8,4I5,5F17.5)

c...change the sign of z to dpmjet convention and transform to 
c...nuclear rest frame      
c     P3=pgamma*(PHEP(3,J)-pbeta*PHEP(4,J))
c     P4=pgamma*(PHEP(4,J)-pbeta*PHEP(3,J))
c     PHEP(3,J)=-P3
c     PHEP(4,J)=P4
c...find the virtual photon to do LT from lab to gamma c.m.s      
      PHEP(3,J)=-PHEP(3,J)
      IF((IDHEP(J).EQ.22).AND.(ISTHEP(J).EQ.3).AND.
     & (JMOHEP(1,J).EQ.1)) THEN
      GAMM(1)=PHEP(1,J)
      GAMM(2)=PHEP(2,J)
      GAMM(3)=PHEP(3,J)
      GAMM(4)=PHEP(4,J)
      GAMM(5)=PHEP(5,J)
      ENDIF
      ENDDO
c...loop to find exchanged boson end

**************copy entries from pythia to DTEVT1*************
*********transform pythia entries from lab to c.m.s of gamma+p********
      eveBETA(1)=-(PHEP(1,2)+GAMM(1))/(PHEP(4,2)+GAMM(4))
      eveBETA(2)=-(PHEP(2,2)+GAMM(2))/(PHEP(4,2)+GAMM(4))

**!!!!!!!!remember to change the sign of proton beam in LT*****      
      eveBETA(3)=-(PHEP(3,2)+GAMM(3))/(PHEP(4,2)+GAMM(4))

      eveBETA(4)=eveBETA(1)*eveBETA(1)+eveBETA(2)*eveBETA(2)+
     & eveBETA(3)*eveBETA(3)

      GAA=1./SQRT(1-eveBETA(4))
      
      eveBETA(1)=eveBETA(1)*GAA
      eveBETA(2)=eveBETA(2)*GAA
      eveBETA(3)=eveBETA(3)*GAA
*  in the n rest frame rotate virtual photon  angles to +z axis
*...COD=cos(theta) SID=sin(theta) COF=cos(phi) SIF=sin(phi)
      call DT_DALTRA(GAA,eveBETA(1),eveBETA(2),eveBETA(3),
     &GAMM(1),GAMM(2),GAMM(3),GAMM(4),PTOT,P1,P2,P3,P4)
      PTOT=SQRT(P1**2+P2**2+P3**2)
      COD = P3/PTOT
      PPT = SQRT(P1**2+P2**2)
      SID = PPT/PTOT
      IF(P1.GT.ZERO) THEN
         COF = ONE
      ELSE
         COF = -ONE
      ENDIF
      SIF = ZERO      
      IF (PTOT*SID.GT.TINY10) THEN
         COF = P1/(SID*PTOT)
         SIF = P2/(SID*PTOT)
         ANORF = SQRT(COF*COF+SIF*SIF)
         COF = COF/ANORF
         SIF = SIF/ANORF
      ENDIF


c...Collector to get the sum of final state momentums other than 
c...the scattered electron in gamma nucleon c.m.s frame
      PP1=0.
      PP2=0.
      PP3=0.
      PP4=0.

***************from HEPEVT to HKKEVT***************************
      DO J=1,NHEP
         I=J+NPOINT(1)+1
         ISTHKK(I)=ISTHEP(J)
         IDHKK(I)=IDHEP(J)
         IF(JMOHEP(1,J).GE.1) JMOHKK(1,I)=JMOHEP(1,J)+NPOINT(1)+1
         IF(JMOHEP(2,J).GE.1) JMOHKK(2,I)=JMOHEP(2,J)+NPOINT(1)+1
         IF(JDAHEP(1,J).GE.1) JDAHKK(1,I)=JDAHEP(1,J)+NPOINT(1)+1
         IF(JDAHEP(2,J).GE.1) JDAHKK(2,I)=JDAHEP(2,J)+NPOINT(1)+1
**********rotate in nucleon c.m.s frame***********************         
         call DT_DALTRA(GAA,eveBETA(1),eveBETA(2),eveBETA(3),
     &   PHEP(1,J),PHEP(2,J),PHEP(3,J),PHEP(4,J),PTOT,P1,P2,P3,P4)
         PHKK(1,I)=COD*(COF*P1+SIF*P2)-SID*P3
         PHKK(2,I)=SIF*P1-COF*P2
         PHKK(3,I)=SID*(COF*P1+SIF*P2)+COD*P3
         PHKK(4,I)=P4
c         call DT_LTNUC(P3,P4,PHKK(3,I),PHKK(4,I),3)
*********LT from nucleon rest c.m.s**************************
c         call DT_LTRANS(P1,P2,P3,P4,PHKK(1,I),PHKK(2,I),
c     &    PHKK(3,I),PHKK(4,I),1,3)
******rotate to virtual photon directing z+ ******************         
         PHKK(5,I)=PHEP(5,J)
c         PHKK(1,I)=P1
c         PHKK(2,I)=P2

********get the position of particles in nucleon rest frame***         
c... we simply use the position of involved nucleon for all the
c... particles         
         DO M=1,2
c            VHKK(M,I)=VHEP(M,J)+PosNuc(M)
            VHKK(M,I)=PosNuc(M)
         ENDDO
c         VHKK(4,I)=pgamma*VHEP(4,J)-pgamma*VHEP(3,J)+PosNuc(4)
c         VHKK(3,I)=-pgamma*pbeta*VHEP(4,J)+pgamma*VHEP(3,J)
c         VHKK(3,I)=-VHKK(3,I)+PosNuc(3)   
         VHKK(4,I)=PosNuc(4)
         VHKK(3,I)=PosNuc(3)

c...set BAM ID for the particles         
         IDBAM(I)=IDT_ICIHAD(IDHKK(I))
c...change the IS of out e- from 1 to 99 in order to avoid its 
c...interaction in cascade
         IF( (ISTHEP(J).EQ.1).AND.(IDHEP(J).EQ.11).AND.
     & (JMOHEP(1,J).EQ.3) ) THEN
            ISTHKK(I)=99
            JMOHKK(1,I)=JMOHEP(1,J)
         ENDIF
         NHKK=NHKK+1

c...collect final state momentum in gamma*p cms frame
         IF( ISTHKK(I).EQ.1 ) THEN
            PP1=PHKK(1,I)+PP1
            PP2=PHKK(2,I)+PP2
            PP3=PHKK(3,I)+PP3
            PP4=PHKK(4,I)+PP4
c            print*,'I=',I,' ID=',IDHKK(I),' IS=',ISTHKK(I)
c            print*,'BAM=',IDBAM(I),' P1=',PHKK(1,I),' P2=',PHKK(2,I)
c            print*,'NOBAM=',NOBAM(I),' P3=',PHKK(3,I),' P4=',PHKK(4,I)
         ENDIF
c      WRITE(89,997) I,ISTHKK(I),IDHKK(I),JMOHKK(1,I),
c     &          JMOHKK(2,I),JDAHKK(1,I),JDAHKK(2,I),
c     &          PHKK(1,I),PHKK(2,I),PHKK(3,I),PHKK(4,I),
c     &              PHKK(5,I)
c  997      FORMAT(I5,I5,I8,4I5,5F17.5)
      ENDDO

c      print*,'PP1=',PP1,' PP2=',PP2,' PP3=',PP3,' PP4=',PP4
      call DT_DALTRA(GAA,eveBETA(1),eveBETA(2),eveBETA(3),
     &PF(1),PF(2),PF(3),PF(4),PTOT,P1,P2,P3,P4)
      PF(1)=COD*(COF*P1+SIF*P2)-SID*P3
      PF(2)=SIF*P1-COF*P2
      PF(3)=SID*(COF*P1+SIF*P2)+COD*P3
      PF(4)=P4

c      DO M=1,4
c         print*,'P1(',M,')=',PHKK(M,NPOINT(1)+5),'
c     &    P2(',M,')=',PHKK(M,NPOINT(1)+6)
c      ENDDO
c      print*,'NPOINT1=',NPOINT(1)
c      print*,'ID1=',IDHKK(NPOINT(1)+5),' ID2=',IDHKK(NPOINT(1)+6)

      !!!PAUX is used to balance the PFSP used to estimate 
      !!!total final state particle information
      !!!if energy loss, the lost momenta must be saved for 
      !!!to correctly calculate the nucleus remnant mass
      !!!in the DPMJET routines, where only evaporation energy
      !!!should be considered
      PAUX(1)=PHKK(1,NPOINT(1)+5)+PHKK(1,NPOINT(1)+6)-PP1
      PAUX(2)=PHKK(2,NPOINT(1)+5)+PHKK(2,NPOINT(1)+6)-PP2
      PAUX(3)=PHKK(3,NPOINT(1)+5)+PHKK(3,NPOINT(1)+6)-PP3
      PAUX(4)=PHKK(4,NPOINT(1)+5)+PHKK(4,NPOINT(1)+6)-PP4
c      print*,'PA1=',PAUX(1),' PA2=',PAUX(2),' PA3=',PAUX(3),
c     &' PA4=',PAUX(4)
c      print*,'PF1=',PF(1),' PF2=',PF(2),' PF3=',PF(3),' PF4=',PF(4)
c*******test region*************
c     call DT_DALTRA(GAA,eveBETA(1),eveBETA(2),eveBETA(3),
c    &GAMM(1),GAMM(2),GAMM(3),GAMM(4),PTOT,P1,P2,P3,P4)
c     write(87,*) 'before LT'
c     write(87,*) GAMM(1),' ',GAMM(2),' ',GAMM(3),' ',GAMM(4)
c     write(87,*) 'after LT before rotate '
c     write(87,*) P1,' ',P2,' ',P3,' ',P4
c     PP1=COD*(COF*P1+SIF*P2)-SID*P3
c     PP2=SIF*P1-COF*P2
c     PP3=SID*(COF*P1+SIF*P2)+COD*P3
c     PP4=P4
c     write(87,*) 'after rotate'
c     write(87,*) PP1,' ',PP2,' ',PP3,' ',PP4
c     write(87,*) 'from dpmjet'
c     write(87,*) PPG(1),' ',PPG(2),' ',PPG(3),' ',PPG(4)
c     write(87,*) PGAMM(1),' ',PGAMM(2),' ',PGAMM(3),' ',PGAMM(4)
c     write(87,*) 'VIRT ',VIRT,' GAMM(5) ',GAMM(5),' ',' VINT(307)',
c    &VINT(307) 
c     write(87,*) 'rotate back'
c     write(87,*) P1,' ',P2,' ',P3,' ',P4
c     call DT_DALTRA(GAA,-eveBETA(1),-eveBETA(2),-eveBETA(3),
c    &P1,P2,P3,P4,PTOT,PP1,PP2,PP3,PP4)
c     write(87,*) 'LT back'
c     write(87,*) PP1,' ',PP2,' ',PP3,' ',PP4
c****************************************


      LFIRST=.TRUE.
      RETURN

9999  CONTINUE
      IREJ=1

      END


*=====dt_pyout=========================================================
*used for the output of pythia event list and statistics information
      SUBROUTINE DT_PYOUTEP(MODE)     
 
*     input:
*           MODE: 1:reject statistics
*                 2:event output
*                 3:total statistics print

      include 'pythia.inc'              ! All PYTHIA commons blocks
      include "mc_set.inc"
      include "py6strf.inc"
      include "mcRadCor.inc"
      include "radgen.inc"
      include "phiout.inc"

* event history

      PARAMETER (NMXHKK=200000)

      COMMON /DTEVT1/ NHKK,NEVHKK,ISTHKK(NMXHKK),IDHKK(NMXHKK),
     &                JMOHKK(2,NMXHKK),JDAHKK(2,NMXHKK),
     &                PHKK(5,NMXHKK),VHKK(4,NMXHKK),WHKK(4,NMXHKK)

* extended event history
      COMMON /DTEVT2/ IDRES(NMXHKK),IDXRES(NMXHKK),NOBAM(NMXHKK),
     &                IDBAM(NMXHKK),IDCH(NMXHKK),NPOINT(10)
c     &                IHIST(2,NMXHKK)

      PARAMETER (ZERO=0.0D0,ONE=1.0D0,TINY10=1.0D0-10,MAXNCL=260)

* Glauber formalism: collision properties
      COMMON /DTGLCP/ RPROJ,RTARG,BIMPAC,
     &                NWTSAM,NWASAM,NWBSAM,NWTACC,NWAACC,NWBACC


C...Information about LT from lab to nucleon rest frame
      COMMON /LABTONR/ pgamma, pbeta

* lorentz transformation parameter
      COMMON /LTPARA/ BGTA(4), GAMM(5), eveBETA(4), GAA
      COMMON /ROTATE/ COF,COD,SIF,SID

* event flag
      COMMON /DTEVNO/ NEVENT,ICASCA 

* added by liang to store the output event variables 1/20/12
      COMMON /EVTOUT/ XBJOUT,YYOUT,W2OUT,NUOUT,Q2OUT
      DOUBLE PRECISION XBJOUT,YYOUT,W2OUT,NUOUT,Q2OUT

C...Switches for nuclear correction
      COMMON /PYNUCL/ INUMOD,CHANUM,ORDER,genShd
      SAVE /PYNUCL/
      DOUBLE PRECISION INUMOD,CHANUM
      INTEGER ORDER,genShd

* treatment of residual nuclei: 4-momenta
      LOGICAL LRCLPR,LRCLTA
      COMMON /DTRNU1/ PINIPR(5),PINITA(5),PRCLPR(5),PRCLTA(5),
     &                TRCLPR(5),TRCLTA(5),LRCLPR,LRCLTA

* added by liang to check the photon flux 12/28/11
      COMMON /FLCHK/ PFXCHK
      DOUBLE PRECISION PFXCHK

C...output file name definition
      COMMON /OUNAME/ outname

      CHARACTER*256 outname

      DOUBLE PRECISION P1, P2, P3, P4, PTOT, PP1, PP2, PP3, PP4

      integer NEV, NPRT, ievent, genevent, I, tracknr, ltype 
      integer lastgenevent, idum1, idum2, initseed, nrtrack
      REAL trueX, trueW2, trueNu
      DOUBLE PRECISION sqrts, radgamE, radgamp, radgamEnucl
      DOUBLE PRECISION pbeamE, pbeta, pgamma, ebeamE, epznucl
      CHARACTER PARAM*100
      LOGICAL UseLut, GenLut


      INTEGER MODE

      LOGICAL FIRST
      SAVE FIRST
      DATA FIRST /.TRUE./

      pbeamE=sqrt(pbeam**2+massp**2)
      ebeamE=sqrt(ebeam**2+masse**2)
      sqrts=sqrt((pbeamE+ebeamE)**2-(pbeam-ebeam)**2)
      trueX =  VINT(307)/VINT(309)/(sqrts)
      trueW2 = massp**2 + VINT(307)*(1/trueX-1)
      trueNu = (trueW2 + VINT(307) - massp**2)/(2.*massp)
      if (mcRadCor_EBrems.gt.0.) then
         radgamEnucl=sqrt(dplabg(1)**2+dplabg(2)**2+dplabg(3)**2)
         radgamE=pgamma*radgamEnucl-pgamma*pbeta*dplabg(3)
         radgamp=-pgamma*pbeta*radgamEnucl+pgamma*dplabg(3)
C         write(*,*) radgamEnucl, radgamE, dplabg(3), radgamp
      else
        radgamEnucl=0D0
        radgamE=0D0
        radgamp=0D0 
      endif

      if ((msti(1).ge.91).and.(msti(1).le.94)) msti(16)=0

      ievent = NEVENT

      GOTO (1,2,3) MODE

c...mode 1 is used to update the reject statistics in pythia
1     CONTINUE      
c      write(99,*),'event ',ievent,' rejected,',' proces=',
c     & msti(1),', X=',XBJOUT,' Q2=',Q2OUT 
         
      RETURN

c...mode 2 is used to output the event list
2     CONTINUE

      IF(FIRST) open(29, file=outname,STATUS='UNKNOWN')

**************check HKKEVT***********************************
      DO  J=1,NHKK
********rotate back from the gamma* +z direction***********
      IF(J.GT.(NPOINT(1)+1)) THEN
        P1=COF*(COD*PHKK(1,J)+SID*PHKK(3,J))+SIF*PHKK(2,J)
        P2=SIF*(COD*PHKK(1,J)+SID*PHKK(3,J))-COF*PHKK(2,J)
        P3=COD*PHKK(3,J)-SID*PHKK(1,J)
        P4=PHKK(4,J)
c**************transform back to the lab frame**************      
      call DT_DALTRA(GAA,-eveBETA(1),-eveBETA(2),-eveBETA(3),
     &   P1,P2,P3,P4,PTOT,PP1,PP2,PP3,PP4)
c      WRITE(89,996) J,ISTHKK(J),IDHKK(J),JMOHKK(1,J),
c     &          JMOHKK(2,J),JDAHKK(1,J),JDAHKK(2,J),
c     &          PP1,PP2,-PP3,PP4, !remember to change the sign of z back
c     &              PHKK(5,J)
c  996      FORMAT(I5,I5,I8,4I5,5F17.5)
      PHKK(1,J)=PP1
      PHKK(2,J)=PP2      
      PHKK(3,J)=-PP3
      PHKK(4,J)=PP4
c     PHKK(3,J)=pgamma*(P3+pbeta*P4)
c     PHKK(4,J)=pgamma*(P4+pbeta*P3)
c...find the exchanged boson and out e- to make it fit root tree making rules
c...in the following steps
      IF((ISTHKK(J).EQ.3).AND.(IDHKK(J).EQ.22).AND.
     &   (JMOHKK(1,J).EQ.(NPOINT(1)+2))) THEN
         IBOSON=J
      ELSEIF(ISTHKK(J).EQ.99) THEN
         ISTHKK(J)=1
         JMOHKK(1,J)=3
         ILEPT=J
      ENDIF
      ENDIF
      ENDDO
*************check HKKEVT end***************************      


      genevent=NGEN(0,3)-lastgenevent
      tracknr = NHKK
      if (mcRadCor_EBrems.gt.0.) then
         nrtrack=tracknr+1
      else
         nrtrack=tracknr
      endif


c...print a title for the event file
      IF(FIRST) THEN
        write(29,*)' PYTHIA EVENT FILE '
        write(29,*)'============================================'
        write(29,30) 
30      format('I, ievent, genevent, subprocess, nucleon,
     &  targetparton, xtargparton, beamparton, xbeamparton,
     &  thetabeamprtn, truey, trueQ2, truex, trueW2, trueNu, leptonphi, 
     &  s_hat, t_hat, u_hat, pt2_hat, Q2_hat, F2, F1, R, sigma_rad, 
     &  SigRadCor, EBrems, photonflux, nrTracks')
        write(29,*)'============================================'

c...use the dpmjet track wide title
        write(29,*)'I  ISTHKK(I)  IDHKK(I)  JMOHKK(1,I)
     & JDAHKK(1,I)  JDAHKK(2,I)  PHKK(1,I)  PHKK(2,I)  PHKK(3,I)
     & PHKK(4,I)  PHKK(5,I)  VHKK(1,I) VHKK(2,I) VHKK(3,I)'

c        write(29,*)' I  K(I,1)  K(I,2)  K(I,3)  K(I,4)  K(I,5)
c     &  P(I,1)  P(I,2)  P(I,3)  P(I,4)  P(I,5)  V(I,1)  V(I,2)  V(I,3)'
        write(29,*)'============================================'
         FIRST=.FALSE.
      ENDIF

***************standard output for event info***************************
         write(29,32) 0, ievent, genevent, msti(1), msti(12), 
     &        msti(16), pari(34), msti(15), pari(33), pari(53), 
c     &        VINT(309), VINT(307), trueX, trueW2, trueNu,
     &        YYOUT, Q2OUT, XBJOUT, W2OUT, NUOUT,
     &        VINT(313), pari(14), pari(15), pari(16), 
     &        pari(18),  pari(22), sngl(py6f2), sngl(py6f1), 
     &        py6r, mcRadCor_Sigrad, mcRadCor_sigcor, radgamEnucl,
     &        VINT(319), nrtrack 
 32      format((I4,1x,$),(I10,1x,$),3(I4,1x,$),(I10,1x,$),f9.6,1x,$,
     &         I12,1x,$,
     &         2(f12.6,1x,$),7(f18.11,3x,$),11(f19.9,3x,$),I12,/)
         write(29,*)'============================================'

***************standard output for particle info************************
c...add 2 beam information at first to fit into root tree making rule      
         I=NPOINT(1)+2   
         write(29,34) 1,21,11,0,I+4,0,
     &        PHKK(1,I),PHKK(2,I),PHKK(3,I),PHKK(4,I),PHKK(5,I),
     &        VHKK(1,I),VHKK(2,I),VHKK(3,I)
c...nuclear beam (if nucleus) needs to be made with modification
         PP1=0
         PP2=0
c         IF(INUMOD.GT.1) THEN
c            PP5 = PINITA(5)
c            call DT_LTNUC(PINITA(3),PINITA(4),PP3,PP4,-3)
c            !!Lorentz Transformation
c            !!E = gama*(E-beta*pz); pz = gamma*(pz-beta*E)
c            P4 = pgamma*PP4 - pgamma*(-pbeta)*PP3
c            P3 = -pgamma*(-pbeta)*PP4 + pgamma*PP3
c         ELSE
            PP5 = PHKK(5,I+1)
            P4 = PHKK(4,I+1)
            P3 = PHKK(3,I+1)
c         ENDIF
         write(29,34) 2,21,2212,0,I+5,0,
     &        PP1,PP2,P3,
     &        P4,PP5,
     &        VHKK(1,I+1),VHKK(2,I+1),VHKK(3,I+1)
c...add the exchanged boson from the 
         write(29,34) 3,21,11,1,ILEPT+4,0,
     &        PHKK(1,ILEPT),PHKK(2,ILEPT),PHKK(3,ILEPT),
     &        PHKK(4,ILEPT),PHKK(5,ILEPT),VHKK(1,ILEPT),
     &        VHKK(2,ILEPT),VHKK(3,ILEPT)
c...add the exchanged boson from the 
         write(29,34) 4,21,22,1,IBOSON+4,0,
     &        PHKK(1,IBOSON),PHKK(2,IBOSON),PHKK(3,IBOSON),
     &        PHKK(4,IBOSON),PHKK(5,IBOSON),VHKK(1,IBOSON),
     &        VHKK(2,IBOSON),VHKK(3,IBOSON)

 
         DO I=1,tracknr
c         if (K(I,3).le.nrtrack) then
c...make the mother daughter relation consistent with 2 beam particles
c...and virtual photon added on         
         IF(I.NE.ILEPT) THEN
               IF(JMOHKK(1,I).GT.0) JMOHKK(1,I)=JMOHKK(1,I)+4
               IF(JDAHKK(1,I).GT.0) JDAHKK(1,I)=JDAHKK(1,I)+4
               IF(JDAHKK(2,I).GT.0) JDAHKK(2,I)=JDAHKK(2,I)+4
         ENDIF
         !!!dump nuclear remnants into final state particles
c         IF((ISTHKK(I).EQ.-1).OR.
c     &    (ISTHKK(I).EQ.1001)) THEN
c               ISTHKK(I)=1
c         ENDIF
         write(29,34) I+4,ISTHKK(I),IDHKK(I),JMOHKK(1,I),JDAHKK(1,I),
     &        JDAHKK(2,I),PHKK(1,I),PHKK(2,I),PHKK(3,I),PHKK(4,I),
     &        PHKK(5,I),
     &        VHKK(1,I),VHKK(2,I),VHKK(3,I)
c         endif
         ENDDO
c         if (mcRadCor_EBrems.gt.0.) then
c            write(29,34) nrtrack, 55, 22, 1, 0, 0,
c     &      sngl(dplabg(1)),sngl(dplabg(2)),sngl(-radgamp),
c     &      sngl(radgamE), 0., 0., 0., 0.
c         endif
 34      format(2(I6,1x,$),I10,1x,$,3(I8,1x,$),5(f15.6,1x,$),
     &      3(e15.6,1x,$)/)
         write(29,*)'=============== Event finished ==============='
      

         lastgenevent=NGEN(0,3)

c         print*,'output finished'
      RETURN

c...mode 3 is used to print the whole statistics information
3     CONTINUE
         CALL PYSTAT(1)
         CALL PYSTAT(4)

      WRITE(*,*)'The charm mass used is: ', PMAS(4,1)
         
C...Print the Pythia cross section which is needed to get an absolut 
C   normalisation the number is in microbarns
       write(*,*)'==================================================='
       write(*,*)'Pythia total cross section normalisation:',
     &            pari(1)*1000, ' microbarn'
c       write(*,*)'Total Number of generated events pythia', MSTI(5)
       write(*,*)'Total Number of generated events', NEVENT
c       write(*,*)'Total Number of trials', NGEN(0,3)
       write(*,*)'==================================================='
       close(29)

C...Check pdf status       
      call PDFSTA
      
      RETURN

      END 


*=====dt_pyf2qpm=========================================================
*used for the calculation of F2 for pythia events sampling in QPM
*formalism
      SUBROUTINE DT_PYF2QPM(Q2,X,F2) 

C...Double precision and integer declarations.
      IMPLICIT DOUBLE PRECISION(A-H, O-Z)
      IMPLICIT INTEGER(I-N)
     
c...Local variables
      DIMENSION XPQ(-25:25)
      DOUBLE PRECISION Q2,F2,X

      INTEGER i,j,iter

      call PYPDFU(2212,X,Q2,XPQ)
      F2=4.0*(XPQ(2)+XPQ(-2)+XPQ(4)+XPQ(-4))/9.0+
     &   (XPQ(1)+XPQ(-1)+XPQ(3)+XPQ(-3))/9.0
   
      RETURN

      END    
       


      SUBROUTINE DT_PYINITPP(Beam1,Beam2,IT,ITZ)

*     input:
*           Beam1      projectile beam energy in lab frame
*           Beam2      target beam energy in lab frame
*           IT       target mass number
*           ITZ      target charge number
c...As the p-p system is already in the cms frame, we don't
c...have to make a lorentz boost. The only thing left is to
c...rotate it to the right direction. So the simplest way is
c...to make the projectile going in the +z direction, while
c...target going in the -z direction, then we must assign
c...Beam1 to z, Beam2 to -z, ulike the case in ep init, but
c...consistent with the internal definition of dpmjet.  

      include 'pythia.inc'              ! All PYTHIA commons blocks
      include "mc_set.inc"
      include "py6strf.inc"

C...Added by liang 1/6/12
C...Switches for nuclear correction
      COMMON /PYNUCL/ INUMOD,CHANUM,ORDER,genShd
      SAVE /PYNUCL/
      DOUBLE PRECISION INUMOD,CHANUM
      INTEGER ORDER,genShd

C...Parameters and switch for energy loss
      DOUBLE PRECISION QHAT
      INTEGER QSWITCH
      COMMON /QUENCH/ QHAT, QSWITCH

C...Information about LT from lab to nucleon rest frame
      COMMON /LABTONR/ pgamma, pbeta
      DOUBLE PRECISION pbeta, pgamma

      DOUBLE PRECISION Beam1,Beam2
      INTEGER IT,ITZ
      DOUBLE PRECISION sqrts
      CHARACTER PARAM*100

C...output file name definition
      COMMON /OUNAME/ outname

c ---------------------------------------------------------------------
c     Run parameter
c ---------------------------------------------------------------------
      integer*4 today(3), now(3)
c---------------------------------------------------------------------
c     ASCII output file
c ---------------------------------------------------------------------
      integer asciiLun
      parameter (asciiLun=29)
      CHARACTER*256 outname

      integer LINP
      parameter ( LINP=28 )
      CHARACTER*256 inputfilename

c---------------------------------------------------------------------
! ... force block data modules to be read
C     external pydata
c ---------------------------------------------------------------------
c ---------------------------------------------------------------------
c     Open ascii input file
c ---------------------------------------------------------------------
       inputfilename='input.data_RHIC-tuneA'
       open(LINP, file=inputfilename,STATUS='UNKNOWN')
       write(*,*) 'the input file is: ', inputfilename


C...Read switch for output file yes or no
       READ(LINP,*) outname
C...Read switch for shadowing
       READ(LINP,*) genShd
C...Read nuclear pdf correction order
       READ(LINP,*) ORDER
C...Read the switch for quenching
       READ(LINP,*) QSWITCH
C...Read q hat
       READ(LINP,*) QHAT
C...Read information for cross section used in radgen
  100  READ(LINP,'(A)',END=200) PARAM
       CALL PYGIVE(PARAM)
       GOTO 100
c ---------------------------------------------------------------------
C...Initialize PYTHIA.      
c ---------------------------------------------------------------------
  200  write(*,*) '*********************************************'
       write(*,*) 'NOW all parameters are read by PYTHIA'
       write(*,*) '*********************************************'
      write(*,*) 'the outputfile will be named: ', outname
      
!     Getting the date and time of the event generation
      call idate(today)   ! today(1)=day, (2)=month, (3)=year
      call itime(now)     ! now(1)=hour, (2)=minute, (3)=second
        
C...The random number in pythia has been silenced. Use only that
C   FLUKA which is initialized in dpmjet
!      
!     Take date as the SEED for the random number generation
!      
!      initseed = today(1) + 10*today(2) + today(3) + now(1) + 5*now(3)
!      write(6,*) 'SEED = ', initseed
!      call rndmq (idum1,idum2,initseed,' ')

      INUMOD=IT
      CHANUM=ITZ

      pbeta=Beam2/sqrt(Beam2**2+PYMASS(2212)**2)
      pgamma=sqrt(Beam2**2+PYMASS(2212)**2)/PYMASS(2212)
      sqrts=sqrt(4*Beam1*Beam2)
      write(*,*) '*********************************************'
      write(*,*) '1st beam energy:', Beam1, 'GeV'
      write(*,*) '2nd beam energy:', Beam2, 'GeV'
      write(*,*) 'resulting sqrt(s):', sqrts, 'GeV'
      write(*,*) '*********************************************'
C     proton is defined in positive z and as target
      P(2,1)=0.0  
      P(2,2)=0.0  
      P(2,3)=-Beam2
C     lepton is defined in negative z and as beam
      P(1,1)=0.0  
      P(1,2)=0.0  
      P(1,3)=Beam1
      call pyinit ('3MOM','p+','p+',WIN)

      IF(QSWITCH.EQ.1) THEN
         print*,'Quenching requested, qhat=',QHAT
         call GenNucDens(int(CHANUM), int(INUMOD))
c...when quenching is used switch off internal parton shower
         MSTP(61)=0
         MSTP(71)=0
      ENDIF

      RETURN
      END


      SUBROUTINE DT_PYEVNTPP(IREJ)

      include 'pythia.inc'              ! All PYTHIA commons blocks
      include "mc_set.inc"
      include "py6strf.inc"

C...Added by liang 1/6/12
C...Switches for nuclear correction
      COMMON /PYNUCL/ INUMOD,CHANUM,ORDER,genShd
      SAVE /PYNUCL/
      DOUBLE PRECISION INUMOD,CHANUM
      INTEGER ORDER,genShd

C...Parameters and switch for energy loss
      DOUBLE PRECISION QHAT
      INTEGER QSWITCH
      COMMON /QUENCH/ QHAT, QSWITCH

C...Information about LT from lab to nucleon rest frame
      COMMON /LABTONR/ pgamma, pbeta

* event history
      PARAMETER (NMXHKK=200000)

      COMMON /DTEVT1/ NHKK,NEVHKK,ISTHKK(NMXHKK),IDHKK(NMXHKK),
     &                JMOHKK(2,NMXHKK),JDAHKK(2,NMXHKK),
     &                PHKK(5,NMXHKK),VHKK(4,NMXHKK),WHKK(4,NMXHKK)

* extended event history
      COMMON /DTEVT2/ IDRES(NMXHKK),IDXRES(NMXHKK),NOBAM(NMXHKK),
     &                IDBAM(NMXHKK),IDCH(NMXHKK),NPOINT(10)

* position of interacted nucleon
      DOUBLE PRECISION PosNuc
      COMMON /NPARINT/ PosNuc(4)

* auxiliary common to do particle energy loss
      double precision PF
      COMMON /CORECT/ PF(4)
      double precision PAUX
      COMMON /PFAUX/ PAUX(4)


      INTEGER J,M
      DOUBLE PRECISION pbeta,pgamma
      DOUBLE PRECISION PP1,PP2,PP3,PP4

      IREJ=0
c...if quenching requested, do not do fragmentation
      if(QSWITCH.EQ.1) MSTJ(1) = 0
999   CALL PYEVNT
      IF(MSTI(61).EQ.1) THEN
         WRITE(*,*) 'go back to PYEVNT call'
         GOTO 999
      ENDIF

c...guess the position of final state particle
c...NPOINT(4), the position where final particle starts is very important!!      
      NPOINT(2)=NPOINT(1)
      NPOINT(3)=NPOINT(1)
      NPOINT(4)=NPOINT(1)+5

********find the position of wounded nucleon
      DO 30 J=1,NHKK      
            IF(ISTHKK(J).EQ.12) THEN
               PosNuc(1)=VHKK(1,J)
               PosNuc(2)=VHKK(2,J)
               PosNuc(3)=VHKK(3,J)
               PosNuc(4)=VHKK(4,J)
               GOTO 31
            ENDIF
30    CONTINUE      
31    CONTINUE
c      write(*,*) 'DPMJET position',PosNuc(1),PosNuc(2),PosNuc(3)

      PF(1)=0
      PF(2)=0
      PF(3)=0
      PF(4)=0

c...do quenching to scattered partons if requested      
      if(QSWITCH.EQ.1) then
         CALL PYROBO(0,0,0.0D0,0.0D0,0.0D0,0.0D0,-pbeta)
         call InterPos ! Pick the position of the interaction in the nuclei
c       CALL PYLIST(2)
         call ApplyQW(QHAT)  ! Compute QW
c       CALL PYLIST(2)
         CALL PYROBO(0,0,0.0D0,0.0D0,0.0D0,0.0D0,pbeta)
c       CALL PYLIST(2)
         stop
         MSTJ(1) = 1
         CALL PYEXEC
      endif

      write(*,*) PF(1),PF(2),PF(3),PF(4)
      PP3=PF(3)
      PP4=PF(4)
      PF(3)=pgamma*(PP3+pbeta*PP4)
      PF(4)=pgamma*(PP4+pbeta*PP3)
      write(*,*) PF(1),PF(2),PF(3),PF(4)

c...Translate PYJETS into HEPEVT event record
      CALL PYHEPC(1)

      PP1=0.
      PP2=0.
      PP3=0.
      PP4=0.
***************from HEPEVT to HKKEVT***************************
      DO J=1,NHEP
         I=J+NPOINT(1)+1
         ISTHKK(I)=ISTHEP(J)
         IDHKK(I)=IDHEP(J)
         IF(JMOHEP(1,J).GE.1) JMOHKK(1,I)=JMOHEP(1,J)+NPOINT(1)+1
         IF(JMOHEP(2,J).GE.1) JMOHKK(2,I)=JMOHEP(2,J)+NPOINT(1)+1
         IF(JDAHEP(1,J).GE.1) JDAHKK(1,I)=JDAHEP(1,J)+NPOINT(1)+1
         IF(JDAHEP(2,J).GE.1) JDAHKK(2,I)=JDAHEP(2,J)+NPOINT(1)+1
********get the position of particles in nucleon rest frame***         
c... we simply use the position of involved nucleon for all the
c... particles         
         DO M=1,4
            VHKK(M,I)=PosNuc(M)
         ENDDO
         DO M=1,5
            PHKK(M,I)=PHEP(M,J)
         ENDDO
c...set BAM ID for the particles         
         IDBAM(I)=IDT_ICIHAD(IDHKK(I))
         NHKK=NHKK+1

         IF( ISTHKK(I).EQ.1 ) THEN
            PP1=PHKK(1,I)+PP1
            PP2=PHKK(2,I)+PP2
            PP3=PHKK(3,I)+PP3
            PP4=PHKK(4,I)+PP4
         ENDIF
      ENDDO

c      DO J=1,NHKK
c         WRITE(89,996) J,ISTHKK(J),IDHKK(J),JMOHKK(1,J),JMOHKK(2,J),
c     &   JDAHKK(1,J),JDAHKK(2,J),PHKK(1,J),PHKK(2,J),PHKK(3,J),
c     &   PHKK(4,J),PHKK(5,J)
c  996      FORMAT(I5,I5,I8,4I5,5F17.5)
c      ENDDO

      print*,'PP1=',PP1,' PP2=',PP2,' PP3=',PP3,' PP4=',PP4
      PAUX(1)=PHKK(1,NPOINT(1))+PHKK(1,NPOINT(1)+1)-PP1
      PAUX(2)=PHKK(2,NPOINT(1))+PHKK(2,NPOINT(1)+1)-PP2
      PAUX(3)=PHKK(3,NPOINT(1))+PHKK(3,NPOINT(1)+1)-PP3
      PAUX(4)=PHKK(4,NPOINT(1))+PHKK(4,NPOINT(1)+1)-PP4
      print*,'PA1=',PAUX(1),' PA2=',PAUX(2),' PA3=',PAUX(3),
     &' PA4=',PAUX(4)

c     print*,NPOINT(1),ISTHKK(NPOINT(1)),IDHKK(NPOINT(1)),
c    & PHKK(3,NPOINT(1))
c     print*,NPOINT(1)+1,ISTHKK(NPOINT(1)+1),IDHKK(NPOINT(1)+1),
c    & PHKK(3,NPOINT(1)+1)

      RETURN
      END

      SUBROUTINE DT_PYOUTPP(MODE)

      include 'pythia.inc'              ! All PYTHIA commons blocks
      include "mc_set.inc"
      include "py6strf.inc"

* event history
      PARAMETER (NMXHKK=200000)

      COMMON /DTEVT1/ NHKK,NEVHKK,ISTHKK(NMXHKK),IDHKK(NMXHKK),
     &                JMOHKK(2,NMXHKK),JDAHKK(2,NMXHKK),
     &                PHKK(5,NMXHKK),VHKK(4,NMXHKK),WHKK(4,NMXHKK)

* extended event history
      COMMON /DTEVT2/ IDRES(NMXHKK),IDXRES(NMXHKK),NOBAM(NMXHKK),
     &                IDBAM(NMXHKK),IDCH(NMXHKK),NPOINT(10)
c     &                IHIST(2,NMXHKK)

* event flag
      COMMON /DTEVNO/ NEVENT,ICASCA 

C...output file name definition
      COMMON /OUNAME/ outname
C...Switches for nuclear correction
      COMMON /PYNUCL/ INUMOD,CHANUM,ORDER,genShd
      SAVE /PYNUCL/
      DOUBLE PRECISION INUMOD,CHANUM
      INTEGER ORDER,genShd


      INTEGER MODE

      LOGICAL FIRST
      SAVE FIRST
      DATA FIRST /.TRUE./

      integer NEV, NPRT, ievent, genevent, I, tracknr
      integer lastgenevent
c---------------------------------------------------------------------
c     ASCII output file
c ---------------------------------------------------------------------
      integer asciiLun
      parameter (asciiLun=29)
      CHARACTER*256 outname

      ievent = NEVENT

      GOTO (1,2,3) MODE

1     CONTINUE
      WRITE(99,*),'event ',ievent,' rejected',' process=',msti(1)
      RETURN


2     CONTINUE
c ---------------------------------------------------------------------
c     Open ascii output file
c ---------------------------------------------------------------------
      open(29, file=outname,STATUS='UNKNOWN')

      genevent=NGEN(0,3)-lastgenevent
      tracknr=NHKK


C   This is what we write in the ascii-file
      IF(FIRST) THEN
        write(29,*)' PYTHIA EVENT FILE '
        write(29,*)'============================================'
        write(29,30) 
30      format('I, ievent, genevent, subprocess, nucleon,
     &  targetparton, xtargparton, beamparton, xbeamparton,
     &  thetabeamprtn, pt2_hat, Q2_hat, nrTracks')
        write(29,*)'============================================'

        write(29,*)' I  K(I,1)  K(I,2)  K(I,3)  K(I,4)  K(I,5)
     &  P(I,1)  P(I,2)  P(I,3)  P(I,4)  P(I,5)  V(I,1)  V(I,2)  V(I,3)'
        write(29,*)'============================================'
         FIRST=.FALSE.
      ENDIF

***************standard output*********************************
         write(29,32) 0,ievent,genevent, msti(1), msti(12), 
     &        msti(16), pari(34), msti(15), pari(33), pari(53), 
     &        pari(18),  pari(22), tracknr
 32      format((I4,1x,$),(I10,1x,$),4(I4,1x,$),f9.6,1x,$,I12,1x,$,
     &           4(f12.6,1x,$),I12,/)
         write(29,*)'============================================'

         DO I=1,tracknr
         write(29,34) I,ISTHKK(I),IDHKK(I),JMOHKK(1,I),JDAHKK(1,I),
     &        JDAHKK(2,I),PHKK(1,I),PHKK(2,I),PHKK(3,I),PHKK(4,I),
     &        PHKK(5,I),VHKK(1,I),VHKK(2,I),VHKK(3,I)
         ENDDO
 34      format(2(I6,1x,$),I10,1x,$,3(I8,1x,$),5(f15.6,1x,$),
     &       3(e15.6,1x,$)/)
         write(29,*)'=============== Event finished ==============='


         lastgenevent=NGEN(0,3)

      RETURN


3     CONTINUE
         CALL PYSTAT(1)
         CALL PYSTAT(4)

      WRITE(*,*)'The charm mass used is: ', PMAS(4,1)
         
C...Print the Pythia cross section which is needed to get an absolut 
C   normalisation the number is in microbarns
       write(*,*)'==================================================='
       write(*,*)'Pythia total cross section normalisation:',
     &            pari(1)*1000, ' microbarn'
       write(*,*)'Total Number of generated events pythia', MSTI(5)
       write(*,*)'Total Number of generated events', NEVENT
       write(*,*)'Total Number of trials', NGEN(0,3)
       write(*,*)'==================================================='
       close(29)

C...Check pdf status       
      call PDFSTA
       
      RETURN

      END
