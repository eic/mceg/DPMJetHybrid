# DPMJetHybrid

A generator to simulate ep/eA DIS events by employing PYTHIA in DPMJet

## Installation instructions 
(on RCF, adapt to your own environment)

#### REQUIRES: 
* FLUKA 
  --> This means we also need a newer gfortran version
       (on RCF, using sPHENIX's) and corresponding versions of
* CERNLIB 
* LHAPDF5
  

## Clone 
```sh
cd $EICDIRECTORY/PACKAGES
git clone git@gitlab.com:eic/mceg/DPMJetHybrid.git
cd $EICDIRECTORY/PACKAGES/DPMJetHybrid
```

## Prepare environment and adapt LD_LIBRARY_PATH
```sh
setenv CERN_ROOT $EICDIRECTORY/gcc83lib
setenv LHAPDF5 $EICDIRECTORY/gcc83lib
setenv LD_LIBRARY_PATH ${LHAPDF5}
source /cvmfs/sphenix.sdcc.bnl.gov/gcc-8.3/opt/sphenix/core/gcc/8.3.0.1-0a5ad/x86_64-centos7/setup.csh
source /cvmfs/sphenix.sdcc.bnl.gov/gcc-8.3/opt/sphenix/core/binutils/2.30-e5b21/x86_64-centos7/setup.csh
```

# Make
```sh
make
```


# Running / Testing

Make and descend into a directory of your choice. From there, do
```sh
setenv CERN_ROOT $EICDIRECTORY/gcc83lib
setenv LHAPDF5 $EICDIRECTORY/gcc83lib
setenv LD_LIBRARY_PATH ${LHAPDF5}
source /cvmfs/sphenix.sdcc.bnl.gov/gcc-8.3/opt/sphenix/core/gcc/8.3.0.1-0a5ad/x86_64-centos7/setup.csh
source /cvmfs/sphenix.sdcc.bnl.gov/gcc-8.3/opt/sphenix/core/binutils/2.30-e5b21/x86_64-centos7/setup.csh
cp $FLUPRO/nuclear.bin .
cp $EICDIRECTORY/PACKAGES/DPMJetHybrid/eAS1* .
cp $EICDIRECTORY/PACKAGES/DPMJetHybrid/*inp .
mkdir outForPythiaMode
ln -s $EICDIRECTORY/PACKAGES/DPMJetHybrid/PyQM

$EICDIRECTORY/PACKAGES/DPMJetHybrid/dpmjetHybrid < eAu_shorttest_Shd1_tau9_kt=ptfrag=0.40.inp
```

This ends with "Note: The following floating-point exceptions are signalling: IEEE_INVALID_FLAG IEEE_DIVIDE_BY_ZERO IEEE_UNDERFLOW_FLAG IEEE_DENORMAL." The output looks reasonable, but it's concerning. See also Notes below.

# Notes

* Creates many many (more than 1000) warnings, such as:

Warning: Legacy Extension: SAVE statement at (1) follows blanket SAVE statement

Warning: Legacy Extension: Blanket SAVE statement at (1) follows previous SAVE statement

Warning: Legacy Extension: Hollerith constant at (1)

Warning: Label 1000 at (1) defined but not used [-Wunused-label]

Warning: Change of value in conversion from ‘REAL(8)’ to ‘REAL(4)’ at (1) [-Wconversion]

Warning: Possible change of value in conversion from REAL(4) to INTEGER(4) at (1) [-Wconversion]

Warning: Array reference at (1) out of bounds (100 > 50) in loop beginning at (2)

Warning: Rank mismatch in argument ‘vdummy’ at (1) (scalar and rank-1) [-Wargument-mismatch]

Warning: Padding of 4 bytes required before ‘qw_wc’ in COMMON ‘quenwei’ at (1); reorder elements or use -fno-align-commons [-Walign-commons]

Warning: Unused variable ‘ranf’ declared at (1) [-Wunused-variable]

Warning: Unused dummy argument ‘itz’ at (1) [-Wunused-dummy-argument]

Warning: Named COMMON block ‘dtmodl’ at (1) shall be of the same size as elsewhere (48 vs 56 bytes)

Warning: Conversion from REAL(8) to default-kind COMPLEX(4) at (1) might lose precision, consider using the KIND argument [-Wconversion]

Warning: Possible change of value in conversion from COMPLEX(8) to REAL(8) at (1) [-Wconversion]

Warning: CHARACTER expression will be truncated in assignment (8/15) at (1) [-Wcharacter-truncation]

Warning: $ should be the last specifier in format at (1)

Warning: Legacy Extension: Comma before i/o item list at (1)

Warning: Nonconforming tab character at (1) [-Wtabs]

Warning: Integer division truncated to constant ‘2’ at (1) [-Winteger-division]


* Some of these are harmless, some are worrying because they were already obsolete in Fortran77, and some are very concerning. Proceed with caution....

* The package comes with its own copy of an (outdated) Pythia 6.4.13, but we can't link to our newer one without 
  going through the code looking for changes both between versions and in customisation.



